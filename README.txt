
-- SUMMARY --

This module really sorts by creation date!

Unlike similar module like Sort Comments or Comment goodness this module sorts
the comments by the creation date ('created' timestamp to be precise).
Not only in flat, but also in threaded display!


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70501 for further information.


-- CONFIGURATION --

Configurable per content type in the comment settings fieldset.
You can also set the order
- Oldest first (DESCending, default)
- Newest first (ASCending)


-- HOW IT WORKS --

It introduces a new column in a seperate table which does pretty much the same
as the standard 'thread' column (See See comment_get_thread() for details).
But instead of using the comment id to sort the threads it uses the creation
timestamp as thread-numbers.
The default comment threading database query is altered to sort with this
column instead.
